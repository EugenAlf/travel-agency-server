var express = require("express");
var app = express();
var bodyParser = require("body-parser");
 /* serves main page */

app.use(bodyParser.urlencoded({ extended: false }));
app.get("/", function(req, res) {
  res.sendfile('index.html')
});
app.get("/add-city", function(req, res) {
  res.send('You just have sent a GET request with next parameteres: ' + JSON.stringify(req.query) + '. By the way, check url in your browser.');
});
app.post("/add-city", function(req, res) {
  res.send('You just have sent a POST request with next parameteres: ' + JSON.stringify(req.body) + '. By the way, your url should be clean now.');
  res.send("OK");
});

/* serves all the static files */
app.get(/^(.+)$/, function(req, res){ 
   console.log('static file request : ' + req.params);
   res.sendfile( __dirname + req.params[0]); 
});

var port = 5000;
app.listen(port, function() {
 console.log("Listening on " + port);
});